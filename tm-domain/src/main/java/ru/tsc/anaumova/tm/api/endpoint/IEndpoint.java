package ru.tsc.anaumova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IEndpoint {

    @NotNull
    String NAMESPACE = "http://endpoint.tm.anaumova.tsc.ru/";

}