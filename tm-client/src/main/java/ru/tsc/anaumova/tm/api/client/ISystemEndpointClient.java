package ru.tsc.anaumova.tm.api.client;

import ru.tsc.anaumova.tm.api.endpoint.ISystemEndpoint;

public interface ISystemEndpointClient extends ISystemEndpoint, IEndpointClient {
}