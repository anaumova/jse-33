package ru.tsc.anaumova.tm.api.client;

import ru.tsc.anaumova.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IProjectEndpoint, IEndpointClient {
}
