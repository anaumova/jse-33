package ru.tsc.anaumova.tm.api.client;

import ru.tsc.anaumova.tm.api.endpoint.IDomainEndpoint;

public interface IDomainEndpointClient extends IDomainEndpoint, IEndpointClient {
}