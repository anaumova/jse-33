package ru.tsc.anaumova.tm.api.client;

import ru.tsc.anaumova.tm.api.endpoint.IProjectTaskEndpoint;

public interface IProjectTaskEndpointClient extends IProjectTaskEndpoint, IEndpointClient {
}