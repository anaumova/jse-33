package ru.tsc.anaumova.tm.api.client;

import ru.tsc.anaumova.tm.api.endpoint.IUserEndpoint;

public interface IUserEndpointClient extends IUserEndpoint, IEndpointClient {
}