package ru.tsc.anaumova.tm.api.client;

import ru.tsc.anaumova.tm.api.endpoint.IAuthEndpoint;

public interface IAuthEndpointClient extends IAuthEndpoint, IEndpointClient {
}