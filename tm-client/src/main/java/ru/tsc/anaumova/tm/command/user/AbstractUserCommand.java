package ru.tsc.anaumova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.api.client.IUserEndpointClient;
import ru.tsc.anaumova.tm.command.AbstractCommand;
import ru.tsc.anaumova.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpointClient getUserEndpointClient() {
        return serviceLocator.getUserEndpointClient();
    }

    protected void showUser(final User user) {
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}