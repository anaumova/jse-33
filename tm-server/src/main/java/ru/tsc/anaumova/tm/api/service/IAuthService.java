package ru.tsc.anaumova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.model.User;

public interface IAuthService {

    @NotNull
    User check(@Nullable String login, @Nullable String password);

}