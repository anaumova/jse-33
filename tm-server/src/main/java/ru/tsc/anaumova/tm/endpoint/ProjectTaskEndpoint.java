package ru.tsc.anaumova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.anaumova.tm.api.service.IProjectTaskService;
import ru.tsc.anaumova.tm.api.service.IServiceLocator;
import ru.tsc.anaumova.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.anaumova.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.anaumova.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.anaumova.tm.dto.response.TaskUnbindFromProjectResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.anaumova.tm.api.endpoint.IProjectTaskEndpoint")
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskBindToProjectRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskUnbindFromProjectRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

}